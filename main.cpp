#include <iostream>

using namespace std;

void evaluateWinOrDraw(char board[3][3], char player);
int spaceToFillCounter();
bool isLegalMove(char board[3][3], int row, int col);

int row, col;
char player = 'X', spaceToFill = '?';
bool gameOver = false;

char board[3][3] = {{spaceToFill, spaceToFill, spaceToFill},
                      {spaceToFill, spaceToFill, spaceToFill},
                      {spaceToFill, spaceToFill, spaceToFill}};

int maxRows = (sizeof(board) / sizeof(board[0]));
int maxCols = (sizeof(board) / sizeof(board[0]));

int main() {
	cout << "///////////////////////////////////////////////////////////////////////////\n";
	cout << "               " << "  Columna 1" << "  Columna 2" << "  Columna 3" << "\n\n";
	cout << "Fila 1            " << "  0          " << "  0          " <<  "  0" <<endl;
 	cout << "Fila 2            " << "  0          " << "  0          " <<  "  0" <<endl;
	cout << "Fila 3            " << "  0          " << "  0          " <<  "  0\n" <<endl;
	cout << "///////////////////////////////////////////////////////////////////////////\n\n";
 

  for (row = 0; row < maxRows; row++) {
    cout << "\n";
    for (col = 0; col < maxCols; col++) {
      cout << board[row][col] << "    ";
    };
  };

  while (!gameOver) {

    cout << "\n\nEres JUGADOR " << player
         << " ingresa la posicion a ocupar (fila columna) -> 1 2 : ";
    cin >> row >> col;
	
    system("clear");

    if (isLegalMove(board, row, col)) {

      board[row - 1][col - 1] = player;

      for (int row = 0; row < maxRows; row++) {
        cout << "\n";
        for (int col = 0; col < maxCols; col++) {
          cout << board[row][col] << "    ";
        };
      };

      evaluateWinOrDraw(board, player);

      if (player == 'X') {
        player = 'O';
      } else {
        player = 'X';
      }

    } else {

      cout << "\n* El movimiento es ilegal: \n\n    - Quiza ya hay un valor "
              "asignado en dicha posicion \n    - La posicion que ingresaste "
              "no existe en el board. Recuerda que el valor debe de estar "
              "entre 1 - 3 para filas y columnas";
    }
  }
  return 1;
}

void evaluateWinOrDraw(char board[3][3], char player) {

  if (board[0][0] == player && board[1][1] == player &&
      board[2][2] == player) {
    gameOver = true;
    cout << "\n\nGana JUGADOR " << player;
  };

  if (board[0][2] == player && board[1][1] == player &&
      board[2][0] == player) {
    gameOver = true;
    cout << "\n\nGana JUGADOR " << player;
  };

  if (board[0][0] == player && board[1][0] == player &&
      board[2][0] == player) {
    gameOver = true;
    cout << "\n\nGana JUGADOR " << player;
  };

  if (board[0][1] == player && board[1][1] == player &&
      board[2][1] == player) {
    gameOver = true;
    cout << "\n\nGana JUGADOR " << player;
  };

  if (board[0][2] == player && board[1][2] == player &&
      board[2][2] == player) {
    gameOver = true;
    cout << "\n\nGana JUGADOR " << player;
  };

  if (board[0][0] == player && board[0][1] == player &&
      board[0][2] == player) {
    gameOver = true;
    cout << "\n\nGana JUGADOR " << player;
  };

  if (board[1][0] == player && board[1][1] == player &&
      board[1][2] == player) {
    gameOver = true;
    cout << "\n\nGana JUGADOR " << player;
  };

  if (board[2][0] == player && board[2][1] == player &&
      board[2][2] == player) {
    gameOver = true;
    cout << "\n\nGana JUGADOR " << player;
  };

  if (spaceToFillCounter() == 0) {
    gameOver = true;
    cout << "\n\nEmpate!!!!!!!!!";
  }
}

int spaceToFillCounter() {
  int accspaceToFill = 0;

  for (row = 0; row < maxRows; row++) {
    for (col = 0; col < maxCols; col++) {
      if (board[row][col] == spaceToFill) {
        accspaceToFill++;
      };
    };
  };

  return accspaceToFill;
}

bool isLegalMove(char board[3][3], int row, int col) {
  if (board[row - 1][col - 1] != spaceToFill || row > maxRows ||
      col > maxCols) {
    return false;
  }

  return true;
}
